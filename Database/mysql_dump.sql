CREATE TABLE `Chats` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	`Type` INT NOT NULL AUTO_INCREMENT,
	`Creator` INT NOT NULL,
	`CreationDate` DATETIME NOT NULL,
	`PicturePath` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`Id`)
);

CREATE TABLE `ChatTypes` (
	`Id` INT NOT NULL AUTO_INCREMENT UNIQUE,
	`Type` VARCHAR(20) NOT NULL UNIQUE,
	PRIMARY KEY (`Id`)
);

CREATE TABLE `Users` (
	`Id` VARCHAR(255) NOT NULL AUTO_INCREMENT,
	`DisplayName` VARCHAR(255),
	`RealName` VARCHAR(255),
	`AvatarPath` VARCHAR(255),
	`Type` INT,
	`Group` INT NOT NULL,
	PRIMARY KEY (`Id`)
);

CREATE TABLE `UserTypes` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	`Type` VARCHAR(255) NOT NULL UNIQUE,
	PRIMARY KEY (`Id`)
);

CREATE TABLE `Chats_Participants` (
	`Chat_Id` INT NOT NULL,
	`Participant_Id` INT NOT NULL,
	PRIMARY KEY (`Chat_Id`)
);

CREATE TABLE `Chats_Messages` (
	`Chat_Id` INT NOT NULL,
	`Message_Id` INT NOT NULL
);

CREATE TABLE `Messages` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	`Sender` INT NOT NULL,
	`TextContent` VARCHAR(255),
	`SendTime` DATETIME NOT NULL,
	`EditTime` DATETIME,
	PRIMARY KEY (`Id`)
);

CREATE TABLE `Messages_Recipients` (
	`Message_Id` INT NOT NULL,
	`Recipient_Id` INT NOT NULL,
	PRIMARY KEY (`Message_Id`)
);

CREATE TABLE `Messages_Attachments` (
	`Message_Id` INT NOT NULL,
	`Attachment_Id` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`Message_Id`)
);

CREATE TABLE `Attachments` (
	`Id` VARCHAR(255) NOT NULL AUTO_INCREMENT,
	`LifeTimeSeconds` INT,
	`PhysicalFileName` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`Id`)
);

CREATE TABLE `Groups` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	`Name` VARCHAR(255) NOT NULL,
	`Department` INT NOT NULL,
	PRIMARY KEY (`Id`)
);

CREATE TABLE `Departments` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	`Name` VARCHAR(255) NOT NULL,
	`Description` VARCHAR(255),
	PRIMARY KEY (`Id`)
);

ALTER TABLE `Chats` ADD CONSTRAINT `Chats_fk0` FOREIGN KEY (`Id`) REFERENCES ``(``);

ALTER TABLE `Chats` ADD CONSTRAINT `Chats_fk1` FOREIGN KEY (`Type`) REFERENCES `ChatTypes`(`Id`);

ALTER TABLE `Chats` ADD CONSTRAINT `Chats_fk2` FOREIGN KEY (`Creator`) REFERENCES `Users`(`Id`);

ALTER TABLE `Users` ADD CONSTRAINT `Users_fk0` FOREIGN KEY (`Type`) REFERENCES `UserTypes`(`Id`);

ALTER TABLE `Users` ADD CONSTRAINT `Users_fk1` FOREIGN KEY (`Group`) REFERENCES `Groups`(`Id`);

ALTER TABLE `Chats_Participants` ADD CONSTRAINT `Chats_Participants_fk0` FOREIGN KEY (`Chat_Id`) REFERENCES `Chats`(`Id`);

ALTER TABLE `Chats_Participants` ADD CONSTRAINT `Chats_Participants_fk1` FOREIGN KEY (`Participant_Id`) REFERENCES `Users`(`Id`);

ALTER TABLE `Chats_Messages` ADD CONSTRAINT `Chats_Messages_fk0` FOREIGN KEY (`Chat_Id`) REFERENCES `Chats`(`Id`);

ALTER TABLE `Chats_Messages` ADD CONSTRAINT `Chats_Messages_fk1` FOREIGN KEY (`Message_Id`) REFERENCES `Messages`(`Id`);

ALTER TABLE `Messages` ADD CONSTRAINT `Messages_fk0` FOREIGN KEY (`Sender`) REFERENCES `Users`(`Id`);

ALTER TABLE `Messages_Recipients` ADD CONSTRAINT `Messages_Recipients_fk0` FOREIGN KEY (`Message_Id`) REFERENCES `Messages`(`Id`);

ALTER TABLE `Messages_Recipients` ADD CONSTRAINT `Messages_Recipients_fk1` FOREIGN KEY (`Recipient_Id`) REFERENCES `Users`(`Id`);

ALTER TABLE `Messages_Attachments` ADD CONSTRAINT `Messages_Attachments_fk0` FOREIGN KEY (`Message_Id`) REFERENCES `Messages`(`Id`);

ALTER TABLE `Messages_Attachments` ADD CONSTRAINT `Messages_Attachments_fk1` FOREIGN KEY (`Attachment_Id`) REFERENCES `Attachments`(`Id`);

ALTER TABLE `Groups` ADD CONSTRAINT `Groups_fk0` FOREIGN KEY (`Department`) REFERENCES `Departments`(`Id`);

