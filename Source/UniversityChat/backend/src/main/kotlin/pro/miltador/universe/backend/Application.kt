package pro.miltador.universe.backend

import org.jetbrains.ktor.application.Application
import org.jetbrains.ktor.application.call
import org.jetbrains.ktor.application.install
import org.jetbrains.ktor.features.*
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.locations.Locations
import org.jetbrains.ktor.locations.location
import org.jetbrains.ktor.logging.CallLogging
import org.jetbrains.ktor.routing.Routing
import org.jetbrains.ktor.util.hex
import pro.miltador.universe.backend.dao.UniverseDatabase
import java.util.concurrent.Executors
import javax.crypto.Mac

/**
 * @author: miltador
 * @created: 6/13/17
 */

@location("/api/login")
class Login(val email: String = "", val password: String = "")

@location("/api/login/oauth2/{type?}")
class LoginOAuth2(val type: String = "")

@location("/user/{user}")
data class UserPage(val user: String)

@location("/register")
data class Register(val userId: String = "", val displayName: String = "", val email: String = "", val password: String = "", val error: String = "")

@location("/logout")
class Logout()



private val exec = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 4)

fun Application.UniverseBackendApplication() {
    val dao = UniverseDatabase(/*JDBCConnection.Companion.create(H2Dialect, pool)*/)
    dao.init()

    install(DefaultHeaders)
    install(CallLogging)
    install(ConditionalHeaders)
    install(PartialContentSupport)
    install(Compression)
    install(Locations)
    install(StatusPages) {
        exception<NotImplementedError> { call.respond(HttpStatusCode.NotImplemented) }
    }
    install(Routing) {
        styles()
        login(dao, exec)
        userPage(dao)
        register(dao)
    }

    fun hash(password: String): String {
        val hmac = Mac.getInstance("HmacSHA1")
        hmac.init(hmacKey)
        return hex(hmac.doFinal(password.toByteArray(Charsets.UTF_8)))
    }
}



