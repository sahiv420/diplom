package pro.miltador.universe.backend

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.plus.Plus
import org.eclipse.egit.github.core.client.GitHubClient
import org.eclipse.egit.github.core.service.UserService
import org.jetbrains.ktor.application.ApplicationCall
import org.jetbrains.ktor.application.call
import org.jetbrains.ktor.application.feature
import org.jetbrains.ktor.auth.*
import org.jetbrains.ktor.client.DefaultHttpClient
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.http.HttpMethod
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.locations.Locations
import org.jetbrains.ktor.locations.location
import org.jetbrains.ktor.locations.oauthAtLocation
import org.jetbrains.ktor.locations.post
import org.jetbrains.ktor.request.host
import org.jetbrains.ktor.request.port
import org.jetbrains.ktor.response.contentType
import org.jetbrains.ktor.response.respondText
import org.jetbrains.ktor.routing.Route
import org.jetbrains.ktor.routing.param
import org.jetbrains.ktor.util.hex
import pro.miltador.universe.backend.dao.DAOFacade
import pro.miltador.universe.backend.model.User
import java.util.concurrent.ExecutorService
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec


/**
 * @author: miltador
 * @created: 6/14/17
 */

private val loginProviders = listOf(
        OAuthServerSettings.OAuth2ServerSettings(
                name = "github",
                authorizeUrl = "https://github.com/login/oauth/authorize",
                accessTokenUrl = "https://github.com/login/oauth/access_token",
                clientId = System.getenv("UNIVERSE_GITHUB_CLIENT_ID").orEmpty(),
                clientSecret = System.getenv("UNIVERSE_GITHUB_CLIENT_SECRET").orEmpty(),
                defaultScopes = listOf("user")
        ),
        OAuthServerSettings.OAuth2ServerSettings(
                name = "google",
                authorizeUrl = "https://accounts.google.com/o/oauth2/auth",
                accessTokenUrl = "https://www.googleapis.com/oauth2/v3/token",
                requestMethod = HttpMethod.Post,

                clientId = System.getenv("UNIVERSE_GOOGLE_CLIENT_ID").orEmpty(),
                clientSecret = System.getenv("UNIVERSE_GOOGLE_CLIENT_SECRET").orEmpty(),
                defaultScopes = listOf(
                        "https://www.googleapis.com/auth/userinfo.email")
        )
).associateBy { it.name }

val mapper = jacksonObjectMapper()
val hashKey = hex("6819b57a326945c1968f45236589")
val hmacKey = SecretKeySpec(hashKey, "HmacSHA1")
fun hash(password: String): String {
    val hmac = Mac.getInstance("HmacSHA1")
    hmac.init(hmacKey)
    return hex(hmac.doFinal(password.toByteArray(Charsets.UTF_8)))
}

fun Route.login(dao: DAOFacade, exec: ExecutorService) {
    post<Login> {
        var login = when {
            !it.email.contains("@") -> null
            it.password.length < 6 -> null
            else -> dao.user(it.email, hash(it.password))
        }

        if (login == null) {
            val newUser = User("1", it.email, "Va", hash(it.password))
            dao.createUser(newUser)
            login = newUser
            //call.respondError("Invalid username or password")
        }
        call.respondJson(login)
    }
    location<LoginOAuth2> {
        authentication {
            oauthAtLocation<LoginOAuth2>(DefaultHttpClient, exec,
                    providerLookup = { loginProviders[it.type] },
                    urlProvider = { _, p -> redirectUrl(LoginOAuth2(p.name), false) })
        }

        param("error") {
            handle {
                call.respondError(call.parameters.getAll("error").toString().orEmpty())
            }
        }

        handle {
            val principal = call.authentication.principal<OAuthAccessTokenResponse>()
            if (principal != null && principal is OAuthAccessTokenResponse.OAuth2) {
                try {
                    val loginProvider = call.parameters["type"]
                    if (!loginProviders.containsKey(loginProvider)) {
                        call.respondError("Unknown login provider")
                    }

                    when (loginProvider) {
                        "github" -> {
                            val client = GitHubClient()
                            client.setOAuth2Token(principal.accessToken)
                            val userService = UserService(client)
                            val user = userService.user
                            call.respondJson(user)
                        }
                        "google" -> {
                            val httpTransport = GoogleNetHttpTransport.newTrustedTransport()
                            val jsonFactory = JacksonFactory.getDefaultInstance()
                            val credential = GoogleCredential().setAccessToken(principal.accessToken)

                            val plus = Plus.Builder(httpTransport, jsonFactory, credential)
                                    .setApplicationName("Universe")
                                    .build()

                            val profile = plus.people().get("me").execute()
                            call.respondJson(profile)
                        }
                        else -> call.respondError("Unknown login provider")
                    }
                } catch (e: Throwable) {
                    call.respondError(e.toString(), HttpStatusCode.InternalServerError)
                }
            } else {
                call.respondError("Failed to retrieve principal")
            }
        }
    }
}

private fun <T : Any> ApplicationCall.redirectUrl(t: T, secure: Boolean = true): String {
    val hostPort = request.host()!! + request.port().let { port -> if (port == 80) "" else ":$port" }
    val protocol = when {
        secure -> "https"
        else -> "http"
    }
    return "$protocol://$hostPort${application.feature(Locations).href(t)}"
}

suspend private fun <T : Any> ApplicationCall.respondJson(t: T) {
    response.contentType(ContentType.Application.Json)
    respondText(mapper.writeValueAsString(t))
}

suspend private fun ApplicationCall.respondError(message: String,
                                                 statusCode: HttpStatusCode = HttpStatusCode.BadRequest) {
    response.status(statusCode)
    respondText(message)
}
