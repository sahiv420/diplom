package pro.miltador.universe.backend

import org.h2.engine.Session
import pro.miltador.universe.backend.dao.*
import org.jetbrains.ktor.application.*
import org.jetbrains.ktor.http.*
import org.jetbrains.ktor.locations.*
import org.jetbrains.ktor.routing.*
import org.jetbrains.ktor.sessions.*

/**
 * @author: miltador
 * @created: 6/14/17
 */
fun Route.userPage(dao: DAOFacade) {
    get<UserPage> {
        val user = call.sessionOrNull<Session>()?.let { dao.user(it.userId) }
        val pageUser = dao.user(it.user)

        if (pageUser == null) {
            call.respond(HttpStatusCode.NotFound.description("User ${it.user} doesn't exist"))
        } else {
            val chats = dao.userChats(it.user).map { dao.getChatInfo(it) }
            val etag = (user?.userId ?: "") + "_" + chats.map { it.text.hashCode() }.hashCode().toString()

            call.respond(FreeMarkerContent("user.ftl", mapOf("user" to user, "pageUser" to pageUser, "chats" to chats), etag))
        }
    }
}
