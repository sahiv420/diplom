package pro.miltador.universe.backend.chat

data class MemberInfo(val id: String, val name: String)