package pro.miltador.universe.backend.dao

import org.jetbrains.squash.connection.DatabaseConnection
import org.jetbrains.squash.connection.transaction
import org.jetbrains.squash.dialects.h2.H2Connection
import org.jetbrains.squash.expressions.eq
import org.jetbrains.squash.query.from
import org.jetbrains.squash.query.where
import org.jetbrains.squash.results.get
import org.jetbrains.squash.schema.create
import org.jetbrains.squash.statements.insertInto
import org.jetbrains.squash.statements.values
import pro.miltador.universe.backend.model.User
import java.io.Closeable
import java.io.File

/**
 * @author: miltador
 * @created: 6/13/17
 */

interface DAOFacade : Closeable {
    fun init()
    fun user(email: String, hash: String?): User?
    fun createUser(user: User)
}

class UniverseDatabase(val db: DatabaseConnection = H2Connection.createMemoryConnection()) : DAOFacade {
    constructor(dir: File) : this(H2Connection.create("jdbc:h2:file:${dir.canonicalFile.absolutePath}"))

    override fun init() {
        db.transaction {
            databaseSchema().create(Users, Messages)
        }
    }

    override fun user(email: String, hash: String?) = db.transaction {
        from(Users).where { Users.email eq email }.execute()
                .mapNotNull {
                    if (hash == null || it[Users.passwordHash] == hash) {
                        User("1", it[Users.email], it[Users.displayName], it[Users.passwordHash])
                    } else null
                }
                .singleOrNull()
    }

    override fun createUser(user: User) = db.transaction {
        insertInto(Users).values {
            it[id] = user.userId
            it[displayName] = user.displayName
            it[email] = user.email
            it[passwordHash] = user.passwordHash
        }.execute()
    }

    override fun close() {
    }
}