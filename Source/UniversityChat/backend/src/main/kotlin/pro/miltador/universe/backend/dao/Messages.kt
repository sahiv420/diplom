package pro.miltador.universe.backend.dao

import org.jetbrains.squash.definition.TableDefinition
import org.jetbrains.squash.definition.index
import org.jetbrains.squash.definition.primaryKey
import org.jetbrains.squash.definition.varchar

/**
 * @author: miltador
 * @created: 6/13/17
 */
object Messages : TableDefinition() {
    val id = varchar("id", 20).primaryKey()
    val sender = varchar("sender_id", 20).index()
    val receiver = varchar("receiver_id", 20).index()
    val text = varchar("text", 65535)
}