package pro.miltador.universe.backend.dao

import org.jetbrains.squash.definition.TableDefinition
import org.jetbrains.squash.definition.primaryKey
import org.jetbrains.squash.definition.uniqueIndex
import org.jetbrains.squash.definition.varchar

/**
 * @author: miltador
 * @created: 6/13/17
 */
object Users : TableDefinition() {
    val id = varchar("id", 20).primaryKey()
    val email = varchar("email", 128).uniqueIndex()
    val displayName = varchar("display_name", 256)
    val passwordHash = varchar("password_hash", 64)
}