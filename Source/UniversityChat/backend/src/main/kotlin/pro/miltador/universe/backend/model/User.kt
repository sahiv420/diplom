package pro.miltador.universe.backend.model

/**
 * @author: miltador
 * @created: 6/13/17
 */

data class User(val userId: String, val email: String, val displayName: String, val passwordHash: String)