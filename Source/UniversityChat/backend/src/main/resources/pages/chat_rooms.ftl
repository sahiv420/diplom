<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">University chat</a>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li><a href="#about">Rooms</a></li>
        <li><a href="#contact">Contacts</a></li>
        <li><a href="#contact">Profile</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>

<div class="container">
  
  <div class="text-center">
    <h1>Available chats to you</h1>
  </div>
  <ul class="list-group col-md-4">
    <li class="list-group-item"><img src="http://4.bp.blogspot.com/-2q6-6MD75bw/VK1_X6sNRSI/AAAAAAAAB2Y/_eRktII8FRc/s1600/JOB_app.png" width="64" height="64"/><a>PI-47 chat room</a></li>
      <li class="list-group-item"><img src="http://4.bp.blogspot.com/-2q6-6MD75bw/VK1_X6sNRSI/AAAAAAAAB2Y/_eRktII8FRc/s1600/JOB_app.png" width="64" height="64"/><a>Petrov Ivan</a></li>
      <li class="list-group-item"><img src="http://4.bp.blogspot.com/-2q6-6MD75bw/VK1_X6sNRSI/AAAAAAAAB2Y/_eRktII8FRc/s1600/JOB_app.png" width="64" height="64"/><a>Cherkess Anna</a></li>
    <li class="list-group-item"><img src="http://4.bp.blogspot.com/-2q6-6MD75bw/VK1_X6sNRSI/AAAAAAAAB2Y/_eRktII8FRc/s1600/JOB_app.png" width="64" height="64"/><a>Vorotnyuk Svitlana</a></li>
  </ul>
  
</div><!-- /.container -->
<footer><div class="container">ZSTU, (c) 2017</div></footer>