<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">University chat</a>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li><a href="#about">Rooms</a></li>
        <li class="active"><a href="#contact">Contacts</a></li>
        <li><a href="#contact">Profile</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>

<div class="container">
  
  <div class="text-center">
    <h1>Contacts</h1>
  </div>
  <ul class="list-group col-md-12">
   <li class="list-group-item">
     <img src="https://thumbs.dreamstime.com/z/young-student-boy-books-desk-8715091.jpg" width="64" height="64"/>
     <a>Petrov Ivan</a>
     <button style="float: right;" class="btn btn-danger">Delete</button>
    </li>
   <li class="list-group-item"><img src="https://thumbs.dreamstime.com/z/student-girl-beautiful-backpack-books-isolated-white-background-60253353.jpg" width="64" height="64"/><a>Cherkess Anna</a><button style="float: right;"  class="btn btn-danger">Delete</button></li>
   <li class="list-group-item"><img src="https://thumbs.dreamstime.com/z/student-girl-beautiful-backpack-books-isolated-white-background-60253353.jpg" width="64" height="64"/><a>Vorotnyuk Svitlana</a><button style="float: right;"  class="btn btn-danger">Delete</button></li>
  </ul>
  <br/><br/><br/>
  <button class="btn btn-primary">Add contact</button>
  <br/><br/><br/>
</div><!-- /.container -->
<footer><div class="container">ZSTU, (c) 2017</div></footer>