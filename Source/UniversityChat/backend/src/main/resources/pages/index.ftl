<#-- @ftlvariable name="top" type="java.util.List<universe.model.Chat>" -->
<#-- @ftlvariable name="latest" type="java.util.List<universe.model.Rokm>" -->

<#import "template.ftl" as layout />

<@layout.mainLayout title="Welcome">
<div class="posts">
    <h3 class="content-subhead">Top 10</h3>
    <@layout.chats_list chats=top></@layout.chats_list>

    <h3 class="content-subhead">Recent 10</h3>
    <@layout.chats_list chats=latest></@layout.chats_list>
</div>
</@layout.mainLayout>
