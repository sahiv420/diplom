<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">University chat</a>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li><a href="#about">Rooms</a></li>
        <li ><a href="#contact">Contacts</a></li>
        <li class="active"><a href="#contact">Profile</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>

<div class="container">
  
  <div class="text-center">
    <h1>Profile</h1>
  </div>
  <div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <img src="http://placehold.it/380x500" alt="" class="img-rounded img-responsive" />
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <h4>
                            Vasyl Solovei</h4>
                        <small><cite title="San Francisco, USA">Zhytomyr, Ukraine <i class="glyphicon glyphicon-map-marker">
                        </i></cite></small>
                        <p>
                            <i class="glyphicon glyphicon-envelope"></i>iam@miltador.pro
                            <br />
                            <br />
                            <i class="glyphicon glyphicon-gift"></i>June 02, 1996</p>
                        <!-- Split button -->
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary">
                                Edit profile</button>
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span><span class="sr-only">Social</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Change name</a></li>
                                <li><a href="https://plus.google.com/+Jquery2dotnet/posts">Change avatar</a></li>
                            </ul>
                        </div>
                      <div class="btn-group">
                            <button type="button" class="btn btn-primary">
                                Change settings</button>
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span><span class="sr-only">Social</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Change password</a></li>
                                <li><a href="https://plus.google.com/+Jquery2dotnet/posts">Change email</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  <br/><br/><br/>
</div><!-- /.container -->
<footer><div class="container">ZSTU, (c) 2017</div></footer>